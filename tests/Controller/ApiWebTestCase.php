<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

class ApiWebTestCase extends WebTestCase
{
    public const FIXTURES_REFERENCE_RELOAD_MAKER = "\033[34m♻\033[0m"; // blue

    protected static function loadFixtures(): void
    {
        self::bootKernel();
        $app = (new Application(self::$kernel))
            ->find('fixtures:import')
        ;
        $input = new ArrayInput([]);
        $output = new BufferedOutput();
        $app->run($input, $output);

        echo self::FIXTURES_REFERENCE_RELOAD_MAKER;
    }
}
