<?php

namespace App\Tests\Controller;

use App\Entity\Equipment;
use App\Repository\EquipmentRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EquipmentControllerTest extends ApiWebTestCase
{
    /**
     * @dataProvider dataProviderAddItemShouldInsertData
     */
    public function testAddItemShouldInsertData(array $parameters, string $expectedDescription): void
    {
        $client = static::createClient();

        $client->request(
            Request::METHOD_POST,
            '/equipments',
            [],
            [],
            [],
            json_encode($parameters)
        );

        $this->assertSame(Response::HTTP_CREATED, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);

        self::assertSame($parameters['name'], $data['name']);
        self::assertSame($parameters['category'], $data['category']);
        self::assertSame($parameters['number'], $data['number']);
        self::assertSame($expectedDescription, $data['description']);
        self::assertNull($data['updatedAt']);
        self::assertNotEmpty($data['createdAt']);
    }

    public function dataProviderAddItemShouldInsertData(): array
    {
        return [
            [
                [
                    'name' => 'Sumsung S7',
                    'category' => 'Phone',
                    'number' => 'PHS7',
                ],
                '',
            ],
            [
                [
                    'name' => 'Sumsung S7',
                    'category' => 'Phone',
                    'number' => 'PHS7',
                    'description' => null,
                ],
                '',
            ],
            [
                [
                    'name' => 'Sumsung S7',
                    'category' => 'Phone',
                    'number' => 'PHS7',
                    'description' => '',
                ],
                '',
            ],
            [
                [
                    'name' => 'Sumsung S7',
                    'category' => 'Phone',
                    'number' => 'PHS7',
                    'description' => 'good phone galaxy S7',
                ],
                'good phone galaxy S7',
            ],
        ];
    }

    public function testGetItem(): void
    {
        $this->loadFixtures();
        $client = static::createClient();

        $client->request(
            Request::METHOD_GET,
            '/equipments/1'
        );

        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $data = json_decode($client->getResponse()->getContent(), true);
        self::assertSame(1, $data['id']);
        self::assertSame('Sumsung S7', $data['name']);
        self::assertSame('Phone', $data['category']);
        self::assertSame('PHS7', $data['number']);
        self::assertSame('', $data['description']);
        self::assertNull($data['updatedAt']);
        self::assertStringContainsString('2021-08-01T18:48:30+02:00', $data['createdAt']);
    }

    public function testGetItemShouldReturn404(): void
    {
        $this->loadFixtures();
        $client = static::createClient();

        $client->request(
            Request::METHOD_GET,
            '/equipments/0'
        );

        $this->assertSame(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());

        $data = json_decode($client->getResponse()->getContent(), true);

        self::assertArrayHasKey('code', $data);
        self::assertArrayHasKey('message', $data);
        self::assertSame(Response::HTTP_NOT_FOUND, $data['code']);
        self::assertSame('Item not found id : 0', $data['message']);
    }

    public function testGetListItems(): void
    {
        $this->loadFixtures();
        $client = static::createClient();

        $client->request(
            Request::METHOD_GET,
            '/equipments'
        );

        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $data = json_decode($client->getResponse()->getContent(), true);

        self::assertCount(3, $data);

        self::assertSame(1, $data[0]['id']);
        self::assertSame('Sumsung S7', $data[0]['name']);
        self::assertSame('Phone', $data[0]['category']);
        self::assertSame('PHS7', $data[0]['number']);

        self::assertSame(2, $data[1]['id']);
        self::assertSame('Sumsung S8', $data[1]['name']);
        self::assertSame('Phone', $data[1]['category']);
        self::assertSame('PHS8', $data[1]['number']);

        self::assertSame(3, $data[2]['id']);
        self::assertSame('DELL XPS-15', $data[2]['name']);
        self::assertSame('PC', $data[2]['category']);
        self::assertSame('DELXPS159500', $data[2]['number']);
    }

    /**
     * @dataProvider dataProviderGetListItemsShouldReturnBadRequest
     */
    public function testGetListItemsShouldReturnBadRequest(array $parameters, string $expectedMessage): void
    {
        $this->loadFixtures();
        $client = static::createClient();

        $client->request(
            Request::METHOD_GET,
            '/equipments',
            $parameters
        );

        $this->assertSame(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());

        $data = json_decode($client->getResponse()->getContent(), true);

        self::assertArrayHasKey('code', $data);
        self::assertArrayHasKey('message', $data);
        self::assertSame(Response::HTTP_BAD_REQUEST, $data['code']);
        self::assertSame($expectedMessage, $data['message']);
    }

    public function dataProviderGetListItemsShouldReturnBadRequest(): array
    {
        return [
          [
              ['offset' => 0, 'limit' => -1],
              'Parameter "limit" of value "-1" violated a constraint "Parameter \'limit\' value, does not match requirements \'\d+\'"',
          ],
            [
                ['offset' => -1, 'limit' => 10],
                'Parameter "offset" of value "-1" violated a constraint "Parameter \'offset\' value, does not match requirements \'\d+\'"',
            ],
        ];
    }

    public function testDeleteItem(): void
    {
        $this->loadFixtures();

        $imageRepository = self::$container->get(EquipmentRepository::class);
        $item = $imageRepository->findOneById(1);

        self::assertInstanceOf(Equipment::class, $item);

        $client = static::createClient();

        $client->request(
            Request::METHOD_DELETE,
            '/equipments/1'
        );

        $this->assertSame(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());

        $data = json_decode($client->getResponse()->getContent(), true);
        self::assertNull($data);

        $itemDeleted = $imageRepository->findOneById(1);
        self::assertNull($itemDeleted);
    }

    public function testUpdateItem(): void
    {
        $this->loadFixtures();

        $dataQuery = [
            'name' => 'updated name',
            'category' => 'PC',
            'number' => 'PCUN',
            'description' => 'updated description',
        ];

        $client = static::createClient();

        $client->request(
            Request::METHOD_PATCH,
            '/equipments/1',
            [],
            [],
            [],
            json_encode($dataQuery)
        );

        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);

        self::assertSame(1, $data['id']);
        self::assertSame($dataQuery['name'], $data['name']);
        self::assertSame($dataQuery['category'], $data['category']);
        self::assertSame($dataQuery['number'], $data['number']);
        self::assertSame($dataQuery['description'], $data['description']);
        self::assertNotNull($data['updatedAt']);
        self::assertSame('2021-08-01T18:48:30+02:00', $data['createdAt']);
    }

    public function testUpdateItemShouldReturn404(): void
    {
        $this->loadFixtures();

        $dataQuery = [
            'name' => 'updated name',
            'category' => 'PC',
            'number' => 'PCUN',
            'description' => 'updated description',
        ];

        $client = static::createClient();

        $client->request(
            Request::METHOD_PATCH,
            '/equipments/0',
            [],
            [],
            [],
            json_encode($dataQuery)
        );

        $this->assertSame(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);

        self::assertSame(Response::HTTP_NOT_FOUND, $data['code']);
        self::assertSame('Item not found id : 0', $data['message']);
    }
}
