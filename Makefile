.PHONY: help build start stop shell nginx purge check-forbidden-method yamllint phpcsfixer phpstan phpunit cache-clear

.DEFAULT_GOAL := help

UID:=$(shell id -u)
GID:=$(shell id -g)
CC:=docker-compose

help:
	@fgrep -h "###" $(MAKEFILE_LIST) | fgrep -v fgrep | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build:				_docker-user ./docker-compose.yml ./docker-compose.override.yml.dist ## Build Docker images
	@echo '\033[1;42m/!\ In case of a build error, relaunch the builder. All the Docker stack is functional, take a look on your own configuration.\033[0m';
	@if [ -f ./docker-compose.override.yml ]; \
	then \
		echo '\033[1;41m/!\ The ./docker-compose.override.yml already exists. So delete it, if you want to reset it.\033[0m'; \
	else \
		cp ./docker-compose.override.yml.dist ./docker-compose.override.yml; \
		echo '\033[1;42m/!\ The ./docker-compose.override.yml was just created. Feel free to put your config in it.\033[0m'; \
	fi
	$(CC) build --force-rm

start: ### Start infrastructure and wait for php service to be ready
	$(CC) -f docker-compose.yml -f docker-compose.override.yml up -d --remove-orphans

shell: ### Run into php container
	$(CC) exec php sh


composer-clear-cache:
	$(CC) exec -T php composer clear-cache

composer-install: ### install composer
	$(CC) exec -T php composer install --prefer-dist --no-progress
	$(CC) exec -T php bin/console ca:cl --env=test
	$(CC) exec -T php bin/console ca:cl --env=dev

stop: ### Stop all dockerised local dev environment
	$(CC) -f docker-compose.yml -f docker-compose.override.yml stop

purge: ### Purge all dockerised local dev environment
	($(CC) kill && $(CC) down --volumes --remove-orphans) || true

yamllint: ### Execute yaml linter
	$(CC) exec -T php bin/console lint:yaml config/

phpcsfixer: ### Execute phpcsfixer
	$(CC) exec -T php vendor/bin/php-cs-fixer fix

phpstan: ### Execute phpstan
	$(CC) exec -T php vendor/bin/phpstan analyse -l 4 src

phpunit: ### Execute phpunit
	$(CC) exec -T php vendor/bin/simple-phpunit

cache-clear: ### Delete cache DEV+PROD
	$(CC) exec -T php bin/console ca:cl --env=test
	$(CC) exec -T php bin/console ca:cl --env=dev

dev-initialize: ###import fixtures for env dev and test
	$(CC) exec -T php bin/console fixtures:import --env=test
	$(CC) exec -T php bin/console fixtures:import --env=dev

_docker-user:
	@echo "services:" > docker-compose.user.yml
	@echo "    mother:" >> docker-compose.user.yml
	@echo "        user: '$(UID):$(GID)'" >> docker-compose.user.yml
	@echo "        environment:" >> docker-compose.user.yml
	@echo "            USER: root" >> docker-compose.user.yml

swagger: ### update documentation swagger
	$(CC) ./vendor/bin/openapi --format yaml --output ./swagger/swagger.yaml ./swagger/swagger.php src
