CREATE DATABASE IF NOT EXISTS `db_api`;
CREATE DATABASE IF NOT EXISTS `db_api_test`;

# create root user and grant rights
CREATE USER 'root'@'localhost' IDENTIFIED BY 'local';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost';
