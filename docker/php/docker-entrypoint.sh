#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

if [ "$1" = 'php-fpm' ] || [ "$1" = 'php' ] || [ "$1" = 'bin/console' ]; then
	mkdir -p var/cache var/log
  setfacl -R -m u:www-data:rwX -m u:"$(whoami)":rwX var &>/dev/null || chown -R "$(whoami)":"$(whoami)" var/cache var/log
  setfacl -dR -m u:www-data:rwX -m u:"$(whoami)":rwX var &>/dev/null || chmod -R 775 var/cache var/log

	if [ "$APP_ENV" != 'prod' ]; then
		composer install --prefer-dist --no-progress --no-suggest --no-interaction
		bin/console cache:clear
	fi
fi

exec docker-php-entrypoint "$@"
