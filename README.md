# api-symfony

Example of a development environment for a Symfony API project.

API Documentation
-----------------

Details documentation can be found [here](./swagger/swagger.yaml).

Command to generate swagger.yaml file

```shell 
make start 
make shell

 ./vendor/bin/openapi --format yaml --output ./swagger/swagger.yaml ./swagger/swagger.php src
```

Installation
------------

First of all, you need to have Docker and Docker-Compose locally.

We use the Makefile

Retrieve project locally:

```shell 
git clone git@gitlab.com:mhaouari/api-symfony.git
```

Build docker environment:

```make build```

Add host:
```shell
sudo vi /etc/hosts
127.0.0.1       api.symfony.local
```

Usage
----

- Start infrastructure:

```shell
make start
```

- Run the application in browser:

  ````http://api.symfony.local/````


- Adminer:

  ```http://api.symfony.local:8080```


- Stop all dockerised local dev environment:

```shell
make stop
```

- Purge all dockerised local dev environment:
 ```shell
make purge
  ```
Tests
-----

Run unit and fonctional tests:

```shell
make phpunit
```

PHPCsFixer:

```shell
make phpcsfixer
```

PHPStan:

```shell
make phpstan
```

Test if the code contains invalid functions:

````shell
make check-forbidden-method
````
