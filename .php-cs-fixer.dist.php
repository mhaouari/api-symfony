<?php

$finder = (new PhpCsFixer\Finder())
    ->exclude(['swagger'])
    ->in(__DIR__)
    ->exclude('var')
;

return (new PhpCsFixer\Config())
    ->setRules([
        '@Symfony' => true,
        'array_syntax' => ['syntax' => 'short'],
        'ordered_class_elements' => true,
    ])
    ->setFinder($finder)
;
