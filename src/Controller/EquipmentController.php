<?php

namespace App\Controller;

use App\Entity\Equipment;
use App\Form\EquipmentType;
use App\Repository\EquipmentRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class EquipmentController extends ApiAbstractController
{
    /**
     * @Route("/equipments", name="api_equipments_create", methods={"POST"})
     *
     *  @OA\Post(
     *      path="/api/v1/equipments",
     *      tags={"Equipment"},
     *      summary="Add Equipment",
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="name",
     *                      type="string",
     *                      description="name"
     *                  ),
     *                  @OA\Property(
     *                      property="category",
     *                      type="string",
     *                      description="category"
     *                  ),
     *                  @OA\Property(
     *                      property="number",
     *                      type="string",
     *                      description="number"
     *                  ),
     *                  @OA\Property(
     *                      property="description",
     *                      type="string",
     *                      description="description equipment"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Add equipment",
     *          @OA\JsonContent(
     *              type="object",
     *              ref="#/components/schemas/Equipment",
     *          ),
     *      ),
     *      @OA\Response(
     *          response="400",
     *          description="Bad Request",
     *      )
     *  )
     */
    public function create(
        Request $request
    ): JsonResponse {
        $item = new Equipment();
        $form = $this->createForm(EquipmentType::class, $item);
        $form->submit(json_decode($request->getContent(), true));

        if (!$form->isValid()) {
            throw new BadRequestHttpException(sprintf('invalid data: %s', (string) $form->getErrors(true, false)));
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($item);
            $em->flush();
        }

        return $this->renderItem($item, ['equipment_item'], Response::HTTP_CREATED);
    }

    /**
     * @Route("/equipments/{id}", name="api_equipments_get_item", methods={"GET"})
     *
     *  @OA\Get(
     *     path="/api/v1/equipments/{id}",
     *     tags={"Equipment"},
     *     summary="Get Equipment",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Identifier Equipment",
     *         required=true,
     *         @OA\Schema(
     *           type="integer"
     *         ),
     *         example=1,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             type="object",
     *             ref="#/components/schemas/Equipment",
     *         ),
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Equipment not found",
     *     )
     * )
     */
    public function getItem($id): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository(Equipment::class)->findOneBy(['id' => $id]);

        if (!$item instanceof Equipment) {
            throw new NotFoundHttpException(sprintf('Item not found id : %s', $id));
        }

        return $this->renderItem($item, ['equipment_item']);
    }

    /**
     * @Route("/equipments", name="api_equipments_get_list_item", methods={"GET"})
     *
     * @Rest\QueryParam(
     *     name="offset",
     *     requirements="\d+",
     *     default=0,
     *     nullable=true,
     *     strict=true,
     *     description="current page"
     * )
     * @Rest\QueryParam(
     *     name="limit",
     *     requirements="\d+",
     *     default=10,
     *     nullable=true,
     *     strict=true,
     *     description="Max number of items per page."
     * )
     *
     * @OA\Get(
     *     path="/api/v1/equipments",
     *     tags={"Equipment"},
     *     summary="list Equipment",
     *     @OA\Parameter(
     *         name="offset",
     *         in="query",
     *         description="The offset",
     *         required=true,
     *         @OA\Schema(
     *           type="integer"
     *         ),
     *         example=0,
     *     ),
     *     @OA\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Max number of items per page.",
     *         required=true,
     *         @OA\Schema(
     *           type="integer"
     *         ),
     *         example=10,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="List equipment",
     *         @OA\JsonContent(
     *             type="array",
     *              @OA\Items(
     *                  ref="#/components/schemas/Equipment",
     *              )
     *         ),
     *     )
     * )
     */
    public function getList(
        ParamFetcherInterface $paramFetcher,
        EquipmentRepository $repository
    ): JsonResponse {
        $offset = (int) $paramFetcher->get('offset');
        $limit = (int) $paramFetcher->get('limit');

        $items = $repository->findItems($offset, $limit);

        return $this->renderItem($items, ['equipment_list']);
    }

    /**
     * @Route("/equipments/{id}", name="api_equipments_delete_item", methods={"DELETE"})
     *
     * @OA\Delete(
     *     path="/api/v1/equipments/{id}",
     *     tags={"Equipment"},
     *     summary="Delete Equipment",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Identifier equipment",
     *         required=true,
     *         @OA\Schema(
     *           type="integer"
     *         ),
     *         example="1",
     *     ),
     *     @OA\Response(
     *         response="204",
     *         description="Equipment deleted",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Equipment not Found",
     *     )
     * )
     */
    public function deleteItem($id): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository(Equipment::class)->findOneBy(['id' => $id]);

        if (!$item instanceof Equipment) {
            throw new NotFoundHttpException(sprintf('Item not found id : %s', $id));
        }

        $em->remove($item);
        $em->flush();

        return $this->renderItem(null, [], Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/equipments/{id}", name="api_equipments_update_item", methods={"PATCH"})
     *
     * @OA\Patch(
     *     path="/api/v1/equipments/{id}",
     *     tags={"Equipment"},
     *     summary="Update Equipment",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Identifier Equipment",
     *         required=true,
     *         @OA\Schema(
     *           type="integer"
     *         ),
     *         example=1,
     *     ),
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="name",
     *                      type="string",
     *                      description="name"
     *                  ),
     *                  @OA\Property(
     *                      property="category",
     *                      type="string",
     *                      description="category"
     *                  ),
     *                  @OA\Property(
     *                      property="number",
     *                      type="string",
     *                      description="number"
     *                  ),
     *                  @OA\Property(
     *                      property="description",
     *                      type="string",
     *                      description="description equipment"
     *                  ),
     *                  example={"name": "S7", "category": "phone", "number": "GALAXY-S7", "description": "hello description exemple"}
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Update Equipment",
     *         @OA\JsonContent(
     *             type="object",
     *             ref="#/components/schemas/Equipment",
     *         ),
     *     ),
     *     @OA\Response(
     *          response="400",
     *          description="Bad Request",
     *      ),
     *     @OA\Response(
     *         response="404",
     *         description="Equipment not found",
     *     )
     * )
     */
    public function updateItem(Request $request, $id): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository(Equipment::class)->findOneBy(['id' => $id]);

        if (!$item instanceof Equipment) {
            throw new NotFoundHttpException(sprintf('Item not found id : %s', $id));
        }

        $form = $this->createForm(EquipmentType::class, $item);
        $form->submit(json_decode($request->getContent(), true));

        if (!$form->isValid()) {
            throw new BadRequestHttpException(sprintf('invalid data: %s', (string) $form->getErrors(true, false)));
        }

        $em->flush();

        return $this->renderItem($item, ['equipment_item']);
    }
}
