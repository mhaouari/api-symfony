<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ApiAbstractController extends AbstractController
{
    protected SerializerInterface $serializer;

    public function __construct(
        SerializerInterface $serializer
    ) {
        $this->serializer = $serializer;
    }

    protected function renderItem($item, array $groups = [], int $statusCode = Response::HTTP_OK, array $headers = []): JsonResponse
    {
        $content = $this
            ->serializer
            ->serialize($item, 'json', !empty($groups) ? ['groups' => $groups] : [])
        ;

        return new JsonResponse($content, $statusCode, $headers, true);
    }
}
