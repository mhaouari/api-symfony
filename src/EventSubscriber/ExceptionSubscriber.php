<?php

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;

class ExceptionSubscriber implements EventSubscriberInterface
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => [
                ['processException', 10],
                ['logException', 0],
            ],
        ];
    }

    public function processException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        $message = sprintf('Exception: %s', $exception->getFile());

        if ($exception instanceof AuthenticationCredentialsNotFoundException) {
            $status = Response::HTTP_UNAUTHORIZED;
            $message = 'Token not found.';
        } elseif ($exception instanceof HttpException) {
            if ($exception instanceof BadRequestHttpException || $exception instanceof NotFoundHttpException || $exception instanceof AccessDeniedHttpException) {
                $status = $exception->getStatusCode();
                $message = $exception->getMessage();
            }
        } else {
            $status = (0 !== $exception->getCode()) ? $exception->getCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
            $message = 'A Throwable exception has been thrown. Please consult application logs.';
            $this->logger->error(sprintf('%s: %s', $event->getThrowable()->getMessage(), $exception->getFile()));
        }

        $data = [
            'code' => $status,
            'message' => $message,
        ];

        $event->setResponse(new JsonResponse($data, $status));
    }

    public function logException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        if (!$exception instanceof NotFoundHttpException && !$exception instanceof BadRequestHttpException) {
            $this->logger->critical($exception);
        }
    }
}
