<?php

namespace App\Entity;

use App\Repository\EquipmentRepository;
use Doctrine\ORM\Mapping as ORM;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=EquipmentRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @OA\Schema(schema="Equipment")
 */
class Equipment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"equipment_item", "equipment_list"})
     * @OA\Property(property="id", type="integer", example=1)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"equipment_item", "equipment_list"})
     * @OA\Property(property="name", type="string", example="Samsung S7")
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"equipment_item", "equipment_list"})
     * @OA\Property(property="category", type="string", example="Phone")
     */
    private ?string $category;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"equipment_item", "equipment_list"})
     * @OA\Property(property="number", type="string", example="SPS7")
     */
    private string $number;

    /**
     * @ORM\Column(type="text")
     * @Groups({"equipment_item"})
     * @OA\Property(property="description", type="string", example="description equipment")
     */
    private ?string $description = '';

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"equipment_item"})
     * @OA\Property(property="createdAt", type="string", example="2021-08-01T18:48:30+02:00")
     */
    private \DateTime $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"equipment_item"})
     * @OA\Property(property="updatedAt", type="string", example="2021-08-01T18:48:30+02:00")
     */
    private ?\DateTime $updatedAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue(): void
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAt(): void
    {
        $this->updatedAt = new \DateTime();
    }
}
