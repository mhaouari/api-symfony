<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportFixturesCommand extends Command
{
    protected static $defaultName = 'fixtures:import';

    private EntityManagerInterface $entityManager;

    private array $fixtures = [
        'tests/fixtures/equipments.sql',
    ];

    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Import fixtures into DB')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $conn = $this->entityManager->getConnection();

        $dropDbCommand = $this->getApplication()->find('doctrine:database:drop');
        $inputs = new ArrayInput([
            '--force' => true,
        ]);
        $dropDbCommand->run($inputs, $output);

        $createDbCommand = $this->getApplication()->find('doctrine:database:create');
        $inputs = new ArrayInput([]);
        $createDbCommand->run($inputs, $output);

        $migrateCommand = $this->getApplication()->find('doctrine:migrations:migrate');
        $inputs->setInteractive(false);
        $migrateCommand->run($inputs, $output);

        foreach ($this->fixtures as $fixture) {
            $sqlFile = file_get_contents($fixture);

            $statement = $conn->prepare($sqlFile);
            $statement->executeQuery();
        }

        return 0;
    }
}
