ARG PHP_VERSION=7.4

# "php" stage
FROM php:${PHP_VERSION}-fpm-alpine AS php

# PECL
ENV     APCU_VERSION 5.1.18

# Removing APKINDEX warnings
RUN     rm -rf /var/cache/apk/* && \
        rm -rf /tmp/*

RUN     apk update

# Nodejs, native libs and building PHP dependencies
# su-exec > gosu (10kb instead of 1.8MB)
RUN     apk add --update --no-cache \
        git \
        unzip \
        make \
        nodejs-current \
        zlib-dev \
        libzip-dev \
        ca-certificates \
        && apk add --no-cache --virtual .build-deps \
            $PHPIZE_DEPS \
            curl \
            icu-dev \
        && docker-php-ext-install \
            zip \
            pdo_mysql \
        && yes | pecl install apcu-${APCU_VERSION} \
        && yes | pecl install xdebug \
        && docker-php-ext-enable apcu \
        && docker-php-ext-enable opcache \
        && apk add --no-cache su-exec \
        && apk del .build-deps

# Config
RUN rm /usr/local/etc/php-fpm.d/*.conf
COPY docker/php/php-fpm.conf /usr/local/etc/php-fpm.d/php-fpm.conf
COPY docker/php/php.ini /usr/local/etc/php/conf.d/php.ini
COPY --from=composer:2.0.9 /usr/bin/composer /usr/bin/composer
# https://getcomposer.org/doc/03-cli.md#composer-allow-superuser
ENV COMPOSER_ALLOW_SUPERUSER=1

ENV PATH="${PATH}:/root/.composer/vendor/bin"

WORKDIR /app

# build for production
ARG APP_ENV=prod

# do not use .env files in production
RUN echo '<?php return [];' > .env.local.php

CMD ["php-fpm"]

# "nginx" stage
FROM nginx:1.19-alpine AS nginx
RUN apk --update add bash && \
    rm -rf /var/cache/apk/*
COPY docker/nginx/api.conf.template /etc/nginx/conf.d/demo-symfony.conf
COPY docker/nginx/nginx.conf /etc/nginx/nginx.conf
