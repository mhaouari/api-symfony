<?php

use OpenApi\Annotations as OA;

/**
 *
 * @OA\Info (title="API Symfony", version="1.0.0", description="The API Symfony demo.")
 *
 * @OA\Server(
 *     url="https://api.test-domain.com",
 *     description="Server API"
 * )
 *
 * @OA\Tag(name="Equipments")
 *
 */
